<?php

namespace Drupal\multilingual_entities\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

class AutocompleteRouteSubscriber extends RouteSubscriberBase
{

  public function alterRoutes(RouteCollection $collection)
  {
    if ($route = $collection->get('system.entity_autocomplete')) {
      $route->setDefault('_controller', '\Drupal\multilingual_entities\Controller\EntityAutocompleteController::handleAutocomplete');
    }
  }

}
