<?php

namespace Drupal\multilingual_entities\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\Language;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure example settings for this site.
 */
class MultilingualEntitiesConfigForm extends ConfigFormBase
{

  /**
   * Multilingual entities settings.
   *
   * @var string
   */
  const SETTINGS = 'multilingual_entities.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'multilingual_entities';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames()
  {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * The entity type manager.
   *
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Language manager.
   *
   * @var LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param LanguageManagerInterface $languageManager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entityTypeManager, LanguageManagerInterface $languageManager)
  {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entityTypeManager;
    $this->languageManager = $languageManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $languages = $this->languageManager->getLanguages();
    $config = $this->config(static::SETTINGS);

    $form['front_pages'] = [
      '#type' => 'details',
      '#title' => $this->t('Define front page per language.'),
      '#open' => TRUE,
    ];

    /**
     * @var int $language_id
     * @var Language $language
     */
    $node_storage = $this->entityTypeManager->getStorage('node');
    foreach ($languages as $language_id => $language) {
      $page = NULL;
      if ($nid = $config->get('front_page.' . $language_id)) {
        if (!empty($nid) && is_numeric($nid)) {
          $page = $node_storage->load($nid);
        }
      }
      $form['front_pages'][$language_id] = [
        '#type' => 'entity_autocomplete',
        '#title' => $language->getName(),
        '#tags' => FALSE,
        '#target_type' => 'node',
        '#default_value' => $page,
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $language_ids = array_keys($languages = $this->languageManager->getLanguages());
    $values = $form_state->getValues();
    $settings = $this->configFactory->getEditable(static::SETTINGS);
    $settings->delete();
    foreach ($values as $key => $value) {
      if (in_array($key, $language_ids)) {
        $settings->set('front_page.' . $key, $value);
      }
    }
    $settings->save();

    parent::submitForm($form, $form_state);
  }

}
